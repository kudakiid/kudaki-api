<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGunungFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gunung_files', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gunung_id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('gunung_files', function (Blueprint $table) {
            $table->foreign('gunung_id')->references('id')->on('gunungs');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gunung_files', function (Blueprint $table) {
            $table->dropForeign('gunung_files_gunung_id_foreign');
            $table->dropForeign('gunung_files_file_id_foreign');
        });

        Schema::dropIfExists('gunung_files');
    }
}
