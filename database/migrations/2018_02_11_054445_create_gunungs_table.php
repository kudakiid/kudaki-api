<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGunungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gunungs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->integer('ketinggian');
            $table->decimal('latitude');
            $table->decimal('longitude');
            $table->text('deskripsi');
            $table->double('bintang_review')->nullable();
            $table->string('kesulitan')->nullable();
            $table->bigInteger('popular_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gunungs');
    }
}
