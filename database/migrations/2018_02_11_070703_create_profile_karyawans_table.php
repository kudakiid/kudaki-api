<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_karyawans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('toko_id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->string('jabatan');
            $table->string('nama');
            $table->string('telepon');
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->timestamps();
        });

        Schema::table('profile_karyawans', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('toko_id')->references('id')->on('tokos');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile_karyawans', function (Blueprint $table) {
            $table->dropForeign('profile_karyawans_user_id_foreign');
            $table->dropForeign('profile_karyawans_toko_id_foreign');
            $table->dropForeign('profile_karyawans_file_id_foreign');
        });

        Schema::dropIfExists('profile_karyawans');
    }
}
