<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJalursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jalurs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gunung_id')->unsigned();
            $table->integer('file_id')->unsigned();
            $table->string('nama');
            $table->timestamps();
        });

        Schema::table('jalurs', function (Blueprint $table) {
            $table->foreign('gunung_id')->references('id')->on('gunungs');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jalurs', function (Blueprint $table) {
            $table->dropForeign('jalurs_gunung_id_foreign');
            $table->dropForeign('jalurs_file_id_foreign');
        });

        Schema::dropIfExists('jalurs');
    }
}
