<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGunungTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gunung_tracks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gunung_id')->unsigned();
            $table->integer('track_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('gunung_tracks', function (Blueprint $table) {
            $table->foreign('gunung_id')->references('id')->on('gunungs');
            $table->foreign('track_id')->references('id')->on('tracks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gunung_tracks', function (Blueprint $table) {
            $table->dropForeign('gunung_tracks_gunung_id_foreign');
            $table->dropForeign('gunung_tracks_track_id_foreign');
        });

        Schema::dropIfExists('gunung_tracks');
    }
}
