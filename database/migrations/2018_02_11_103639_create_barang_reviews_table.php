<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('barang_id')->unsigned();
            $table->integer('review_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('barang_reviews', function (Blueprint $table) {
            $table->foreign('barang_id')->references('id')->on('barangs');
            $table->foreign('review_id')->references('id')->on('reviews');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('barang_reviews', function (Blueprint $table) {
            $table->dropForeign('barang_reviews_barang_id_foreign');
            $table->dropForeign('barang_reviews_review_id_foreign');
        });

        Schema::dropIfExists('barang_reviews');
    }
}
