<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('keranjang_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->string('id_transaksi')->unique();
            $table->dateTime('issued_pada');
        });

        Schema::table('checkouts', function (Blueprint $table) {
            $table->foreign('keranjang_id')->references('id')->on('keranjangs');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkouts', function (Blueprint $table) {
            $table->dropForeign('checkouts_keranjang_id_foreign');
            $table->dropForeign('checkouts_payment_method_id_foreign');
        });

        Schema::dropIfExists('checkouts');
    }
}
