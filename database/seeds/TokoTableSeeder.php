<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TokoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tokos')->insert([
            ['nama' => 'Sukakaya', 'lokasi_id' => 1, 'buka' => true],
            ['nama' => 'Sukagaya', 'lokasi_id' => 2, 'buka' => false],
            ['nama' => 'Sukamiskin', 'lokasi_id' => 3, 'buka' => false],
            ['nama' => 'Sukahormat', 'lokasi_id' => 4, 'buka' => false],
        ]);
    }
}
