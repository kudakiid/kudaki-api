<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarangTokoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang_tokos')->insert([
            ['toko_id' => 1, 'barang_id' => 2, 'jumlah' => 12, 'jumlah_unit' => 'buah'],
            ['toko_id' => 4, 'barang_id' => 3, 'jumlah' => 5, 'jumlah_unit' => 'buah'],
            ['toko_id' => 3, 'barang_id' => 1, 'jumlah' => 2, 'jumlah_unit' => 'buah'],
            ['toko_id' => 2, 'barang_id' => 4, 'jumlah' => 36, 'jumlah_unit' => 'buah'],
        ]);
    }
}
