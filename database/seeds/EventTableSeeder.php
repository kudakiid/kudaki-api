<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d');

        DB::table('events')->insert([
            ['file_id' => 5, 'nama' => 'Protalk', 'penyelenggara' => 'Prodase', 'nomor_telepon' => '06155445', 'email' => 'prodase@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Cikuray', 'deskripsi' => 'Prodase talk'],
            ['file_id' => 5, 'nama' => 'Buka puasa bersama', 'penyelenggara' => 'EAD', 'nomor_telepon' => '06199887', 'email' => 'ead@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Bromo', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
            ['file_id' => 5, 'nama' => 'BBQ bersama', 'penyelenggara' => 'Daspro', 'nomor_telepon' => '06122332', 'email' => 'daspro@prodase.tech', 'tanggal_selesai' => $date, 'tempat' => 'Padalarang', 'deskripsi' => 'event bukber'],
        ]);
    }
}
