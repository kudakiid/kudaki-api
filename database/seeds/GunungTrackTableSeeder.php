<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GunungTrackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gunung_tracks')->insert([
            ['gunung_id' => 1, 'track_id' => 1],
            ['gunung_id' => 1, 'track_id' => 2],
            ['gunung_id' => 2, 'track_id' => 2]
        ]);
    }
}
