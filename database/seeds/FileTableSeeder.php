<?php

use Illuminate\Database\Seeder;

class FileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $file = new \App\Models\File();
        $file->id = 1;
        $file->nama = "Garp_27_years_ago.png";
        $file->path = "Z:/Punyaku/Projects/ekspedia-api/storage/app/public/Garp_27_years_ago.png";
        $file->type = "image";
        $file->size = 6475;
        $file->save();

        $file2 = new \App\Models\File();
        $file2->id = 2;
        $file2->nama = "Kong_Anime_Infobox.png";
        $file2->path = "Z:/Punyaku/Projects/ekspedia-api/storage/app/public/Kong_Anime_Infobox.png";
        $file2->type = "image";
        $file2->size = 7654;
        $file2->save();

        $file3 = new \App\Models\File();
        $file3->id = 3;
        $file3->nama = "Sengoku_Anime_Pre_Timeskip_Infobox.png";
        $file3->path = "Z:/Punyaku/Projects/ekspedia-api/storage/app/public/Sengoku_Anime_Pre_Timeskip_Infobox.png";
        $file3->type = "image";
        $file3->size = 7614;
        $file3->save();

        $file4 = new \App\Models\File();
        $file4->id = 4;
        $file4->nama = "zhuyahPoTg1odmaneLjUTvkled88I4YaQ5LQEsC8.png";
        $file4->path = "Z:/Punyaku/Projects/ekspedia-api/storage/app/public/zhuyahPoTg1odmaneLjUTvkled88I4YaQ5LQEsC8.png";
        $file4->type = "image";
        $file4->size = 4614;
        $file4->save();

        $file = new \App\Models\File();
        $file->id = 5;
        $file->nama = "hiker.jpg";
        $file->path = "https://images.unsplash.com/photo-1485967249725-2d0b975fa8a2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80";
        $file->type = "image";
        $file->size = 6475;
        $file->save();
    }
}
