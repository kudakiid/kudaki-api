<?php

use Illuminate\Database\Seeder;

class RekomendasiAlatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rekomendasiAlat = new \App\Models\RekomendasiAlat();
        $rekomendasiAlat->file_id = 3;
        $rekomendasiAlat->gunung_id = 1;
        $rekomendasiAlat->nama = "carrier";
        $rekomendasiAlat->jumlah = 1;
        $rekomendasiAlat->satuan = "buah";
        $rekomendasiAlat->orang = 1;
        $rekomendasiAlat->deskripsi = "2 liter";
        $rekomendasiAlat->save();

        $rekomendasiAlat2 = new \App\Models\RekomendasiAlat();
        $rekomendasiAlat2->file_id = 4;
        $rekomendasiAlat2->gunung_id = 2;
        $rekomendasiAlat2->nama = "tenda";
        $rekomendasiAlat2->jumlah = 1;
        $rekomendasiAlat2->satuan = "buah";
        $rekomendasiAlat2->orang = 4;
        $rekomendasiAlat2->deskripsi = "5 x 5";
        $rekomendasiAlat2->save();

        $rekomendasiAlat3 = new \App\Models\RekomendasiAlat();
        $rekomendasiAlat3->file_id = 2;
        $rekomendasiAlat3->gunung_id = 1;
        $rekomendasiAlat3->nama = "sepatu";
        $rekomendasiAlat3->jumlah = 1;
        $rekomendasiAlat3->satuan = "pasang";
        $rekomendasiAlat3->orang = 1;
        $rekomendasiAlat3->deskripsi = "ukuran 43";
        $rekomendasiAlat3->save();
    }
}
