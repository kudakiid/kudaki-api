<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('LokasiTableSeeder');
        $this->call('TokoTableSeeder');
        $this->call('FileTableSeeder');
        $this->call('KategoriTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('ReviewTableSeeder');
        $this->call('GunungTableSeeder');
        $this->call('RekomendasiAlatSeeder');
        $this->call('TrackTableSeeder');
        $this->call('JalurTableSeeder');
        $this->call('GunungTrackTableSeeder');
        $this->call('GunungReviewTableSeeder');
        $this->call('GunungFileTableSeeder');
        $this->call('BrandTableSeeder');
        $this->call('BarangTableSeeder');
        $this->call('BarangReviewTableSeeder');
        $this->call('BarangTokoTableSeeder');
        $this->call('EventTableSeeder');
        $this->call('EventReviewTableSeeder');
        $this->call('PaymentMTableSeeder');
        $this->call('BankTableSeeder');
        $this->call('ProfilePendakiTableSeeder');
    }
}
