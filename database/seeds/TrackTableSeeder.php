<?php

use Illuminate\Database\Seeder;

class TrackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $track = new \App\Models\Track();
        $track->file_id = 1;
        $track->nama = "berlumpur";
        $track->save();

        $track2 = new \App\Models\Track();
        $track2->file_id = 2;
        $track2->nama = "berbatu";
        $track2->save();
    }
}
