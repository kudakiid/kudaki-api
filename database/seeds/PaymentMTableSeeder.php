<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_methods')->insert([
            ['nama'=>'Transfer','deskripsi'=>'none'],
            ['nama'=>'COD','deskripsi'=>'none'],
        ]);
    }
}
