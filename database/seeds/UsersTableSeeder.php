<?php

use App\Models\ProfilePendaki;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $controller = new \App\Http\Controllers\Controller();

        $newUser = new User();
        $newUser->username = "admin";
        $newUser->password = Hash::make("KudakiIDRocks2K18!");
        $newUser->email = "admin@kudaki.id";
        $newUser->save();
        $newUser->role()->attach(2);

        $stringToken = (string)$controller->generateUserToken($newUser);
        $newUser->token = $stringToken;
        $newUser->save();

        $profilePendaki = new ProfilePendaki();
        $profilePendaki->nama = "administrator";
        $profilePendaki->telepon = "0898989898";
        $profilePendaki->user()->associate($newUser);
        $profilePendaki->save();

        $newUser = new User();
        $newUser->username = "qweqwe";
        $newUser->password = Hash::make("password");
        $newUser->email = "qweqwe@qweqwe.com";
        $newUser->save();
        $newUser->role()->attach(3);

        $stringToken = (string)$controller->generateUserToken($newUser);
        $newUser->token = $stringToken;
        $newUser->save();

        $profilePendaki = new ProfilePendaki();
        $profilePendaki->nama = "qweqwe";
        $profilePendaki->telepon = "123123";
        $profilePendaki->user()->associate($newUser);
        $profilePendaki->save();

        $newUser2 = new User();
        $newUser2->username = "asdasd";
        $newUser2->password = Hash::make("password");
        $newUser2->email = "asdasd@asdasd.com";
        $newUser2->save();
        $newUser2->role()->attach(3);

        $stringToken2 = (string)$controller->generateUserToken($newUser2);
        $newUser2->token = $stringToken2;
        $newUser2->save();

        $profilePendaki2 = new ProfilePendaki();
        $profilePendaki2->nama = "asdasd";
        $profilePendaki2->telepon = "456456";
        $profilePendaki2->user()->associate($newUser2);
        $profilePendaki2->save();
    }
}
