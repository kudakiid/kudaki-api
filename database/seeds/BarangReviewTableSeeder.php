<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarangReviewTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang_reviews')->insert([
            ['barang_id' => 1, 'review_id' => 3],
            ['barang_id' => 2, 'review_id' => 4]
        ]);
    }
}
