<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->post('/authentication/pendaki/register', ['as' => 'pendaki.register', 'uses' => 'Auth\RegisterController@registerPendaki']);
    $router->post('/authentication/pendaki/login', ['as' => 'pendaki.login', 'uses' => 'Auth\LoginController@loginPendaki']);

    $router->post('/event/propose', ['as' => 'ajukan_event', 'uses' => 'EventController@ajukanEvent']);
    $router->get('/event/line/semua', ['as' => 'semua_event_line', 'uses' => 'EventController@semuaEventLine']);

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/authentication/pendaki/logout', ['as' => 'pendaki.logout', 'uses' => 'Auth\LoginController@logoutPendaki']);

        $router->group(['middleware' => 'role:pendaki', 'prefix' => 'pendaki'], function () use ($router) {

            $router->post('/profile/edit', ['as' => 'pendaki.edit_profile', 'uses' => 'ProfileController@editProfile']);
            $router->post('/profile/foto/edit', ['as' => 'pendaki.edit_profile_dan_foto', 'uses' => 'ProfileController@editProfileAndPhoto']);
            $router->get('/profile', ['as' => 'pendaki.profile', 'uses' => 'ProfileController@index']);

            $router->post('/review/gunung', ['as' => 'pendaki.review_gunung', 'uses' => 'GunungController@reviewGunung']);
            $router->post('/gunung/cari', ['as' => 'pendaki.cari_gunung', 'uses' => 'GunungController@cariGunung']);
            $router->get('/gunung/popular', ['as' => 'pendaki.gunung_populer', 'uses' => 'GunungController@getPopuler']);
            $router->post('/gunung', ['as' => 'pendaki.gunung', 'uses' => 'GunungController@getGunung']);
            $router->get('/gunung/semua', ['as' => 'pendaki.semua_gunung', 'uses' => 'GunungController@semuaGunung']);

            $router->post('/preparation', ['as' => 'pendaki.preparation', 'uses' => 'GunungController@preparation']);

            $router->get('/porter/semua', ['as' => 'pendaki.semua_porter', 'uses' => 'PorterController@index']);
            $router->post('/porter/cari', ['as' => 'pendaki.cari_porter', 'uses' => 'PorterController@cariPorter']);

            $router->group(['middleware' => 'keranjang', 'prefix' => 'keranjang'], function () use ($router) {
                $router->group(['prefix' => 'barang'], function () use ($router) {
                    $router->post('/tambah', ['as' => 'pendaki.tambah_barang_ke_keranjang', 'uses' => 'KeranjangController@tambahBarang']);
                    $router->post('/edit', ['as' => 'pendaki.edit_barang_di_keranjang', 'uses' => 'KeranjangController@editBarang']);
                    $router->post('/delete', ['as' => 'pendaki.delete_barang_di_keranjang', 'uses' => 'KeranjangController@deleteBarang']);
                    $router->post('/checkout', ['as' => 'pendaki.checkout_keranjang', 'uses' => 'KeranjangController@checkout']);
                    $router->get('/semua', ['as' => 'pendaki.keranjang_semua', 'uses' => 'KeranjangController@index']);
                });
            });
        });

        $router->get('/brand/semua', ['as' => 'semua_brand', 'uses' => 'BarangController@semuaBrand']);
        $router->post('/brand/cari', ['as' => 'cari_brand', 'uses' => 'BarangController@cariBrand']);

        $router->post('/barang/filter', ['as' => 'filter_barang', 'uses' => 'BarangController@filterBarang']);
        $router->get('/barang/semua', ['as' => 'semua_barang', 'uses' => 'BarangController@semuaBarang']);
        $router->post('/barang/kategori', ['as' => 'barang_kategori', 'uses' => 'BarangController@barangKategori']);

        $router->get('/lokasi/semua', ['as' => 'semua_lokasi', 'uses' => 'LokasiController@semuaLokasi']);
        $router->post('/lokasi/cari', ['as' => 'cari_lokasi', 'uses' => 'LokasiController@cariLokasi']);

        $router->get('/event/semua', ['as' => 'semua_event', 'uses' => 'EventController@semuaEvent']);
        $router->post('/event', ['as' => 'event_dari_id', 'uses' => 'EventController@event']);
        $router->get('/event/{id}', ['as' => 'event_dari_id_v2', 'uses' => 'EventController@eventV2']);
        $router->get('/event/line/', ['as' => 'event_line_dari_id_v2', 'uses' => 'EventController@eventLine']);

        $router->get('/payment_method/semua', ['as' => 'semua_payment_method', 'uses' => 'PaymentMethodController@index']);

        $router->get('/bank/semua', ['as' => 'semua_bank', 'uses' => 'BankController@index']);
    });
});