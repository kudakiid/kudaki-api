<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 2:10 PM
 */

namespace App\Listeners;


use App\Events\KeranjangBerubah;
use App\Models\Keranjang;

class KeranjangBertambahListener
{
    public function handle(KeranjangBerubah $keranjangBerubah)
    {
        if ($keranjangBerubah->perubahan == '+') {
            $keranjang = Keranjang::find($keranjangBerubah->keranjangId);
            $keranjang->total_barang = $keranjang->total_barang + $keranjangBerubah->totalBarang;
            $keranjang->total_harga = $keranjang->total_harga + $keranjangBerubah->totalHarga;
            $keranjang->save();
        }
    }
}