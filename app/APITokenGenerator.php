<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 3/2/2018
 * Time: 11:56 AM
 */

namespace App;


use Illuminate\Support\Facades\Crypt;

class APITokenGenerator
{
    public static function generate($keySpace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keySpace, '8bit') - 1;
        for ($i = 0; $i < 6; ++$i) {
            $str .= $keySpace[random_int(0, $max)];
        }

        $apiToken = Crypt::encrypt($str);

        return $apiToken;
    }
}