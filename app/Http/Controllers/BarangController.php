<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 9:58 PM
 */

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Brand;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BarangController extends Controller
{
    public function semuaBarang()
    {
        $barang = Kategori::with(['file', 'barang', 'barang.brand', 'barang.file'])->get();

        if ($barang) {
            return $this->jsonResponse([
                "kategori" => $barang
            ], false, "berhasil mendapatkan semua barang per kategori");
        }

        return $this->jsonResponse(null, true, "tidak ada barang dan kategori", 500);
    }

    public function semuaBrand()
    {
        $semuaBrand = Brand::all();

        if ($semuaBrand) {
            return $this->jsonResponse([
                'brand' => $semuaBrand
            ], false, "berhasil mengambil semua brand");
        }

        return $this->jsonResponse(null, true, "tidak ada brand", 500);
    }

    public function cariBrand(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required'
        ]);

        $brand = Brand::where('nama', 'like', '%' . $request->json("nama") . "%")->get();

        if (count($brand) == 0) {
            return $this->jsonResponse(null, true, "brand dengan nama tersebut tidak ditemukan", 422);
        }
        return $this->jsonResponse([
            "brand" => $brand
        ], false, "berhasil mengambil brand dengan nama tersebut");
    }

    public function filterBarang(Request $request)
    {
        $this->validate($request, [
            'harga_min' => 'required',
            'harga_max' => 'required'
        ]);

        $lokasi = $request->json("lokasi");
        $brand = $request->json("brand");

        if ($lokasi && $brand) {
            $barang = Barang::with(
                [
                    'brand', 'file', 'barangReview.review', 'barangToko.toko.lokasi'
                ])
                ->whereBetween('harga', [$request->json("harga_min"), $request->json("harga_max")])
                ->whereHas('barangToko.toko.lokasi', function ($query) use ($lokasi) {
                    $query->whereIn('nama', $lokasi);
                })
                ->whereHas('brand', function ($query) use ($brand) {
                    $query->whereIn('id', $brand);
                })
                ->get();

            return $this->jsonResponse([
                "barang" => $barang
            ], false, "berhasil mendapatkan barang sesuai dengan min, max, lokasi, dan brand");
        }

        if ($lokasi && !$brand) {
            $barang = Barang::with(
                [
                    'brand', 'file', 'barangReview.review', 'barangToko.toko.lokasi'
                ])
                ->whereBetween('harga', [$request->json("harga_min"), $request->json("harga_max")])
                ->whereHas('barangToko.toko.lokasi', function ($query) use ($lokasi) {
                    $query->whereIn('nama', $lokasi);
                })
                ->get();

            return $this->jsonResponse([
                "barang" => $barang
            ], false, "berhasil mendapatkan barang sesuai dengan min, max, lokasi, dan brand");
        }

        if (!$lokasi && $brand) {
            $barang = Barang::with(
                [
                    'brand', 'file', 'barangReview.review', 'barangToko.toko.lokasi'
                ])
                ->whereBetween('harga', [$request->json("harga_min"), $request->json("harga_max")])
                ->whereHas('brand', function ($query) use ($brand) {
                    $query->whereIn('id', $brand);
                })
                ->get();

            return $this->jsonResponse([
                "barang" => $barang
            ], false, "berhasil mendapatkan barang sesuai dengan min, max, lokasi, dan brand");
        }

        $barang = Barang::with(['brand', 'file', 'barangReview.review'])
            ->whereBetween('harga', [$request->json("harga_min"), $request->json("harga_max")])
            ->get();

        if ($barang) {
            return $this->jsonResponse([
                "barang" => $barang
            ], false, "berhasil mendapatkan barang sesuai dengan min, max, lokasi, dan brand");
        }

        return $this->jsonResponse(null, true, "tidak ditemukan barang berdasarkan filter tersebut", 422);
    }

    public function barangKategori(Request $request)
    {
        $this->validate($request, [
            'kategori_id' => 'required|exists:kategoris,id'
        ]);

        $kategori = Kategori::with(['file', 'barang', 'barang.file'])->where('id', $request->json("kategori_id"))->get();

        if (count($kategori) != 0) {
            return $this->jsonResponse([
                'kategori' => $kategori
            ], false, 'berhasil mengambil barang sesuai kategori');
        }

        return $this->jsonResponse(null, true, 'tidak ada barang di kategori');
    }
}