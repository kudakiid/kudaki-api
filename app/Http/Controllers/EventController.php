<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 10:21 PM
 */

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function semuaEvent()
    {
        $event = Event::with('file')->get();

        if ($event) {
            return $this->jsonResponse([
                'event' => $event
            ], false, "berhasil mendapatkan semua event");
        }

        return $this->jsonResponse(null, true, "tidak ada event", 500);
    }

    public function semuaEventLine()
    {
        $events = Event::where('diterima', 1)->with('file')->get();

        $lineEvent = new \App\Models\Line\Event();

        if ($events->count() != 0) {
            return $this->jsonResponseV2(true, $lineEvent->format($events), "berhasil mendapatkan semua event");
        }

        return $this->jsonResponseV2(false, null, ["tidak ada event"], 500);
    }

    public function ajukanEvent(Request $request)
    {
        $event = new Event();
        $event->nama = $request->input('nama');
        $event->file_id = 5;
        $event->penyelenggara = $request->input('penyelenggara');
        $event->nomor_telepon = $request->input('email');
        $event->tanggal_mulai = $request->input('tanggal_mulai');
        $event->tanggal_selesai = $request->input('tanggal_selesai');
        $event->tempat = $request->input('tempat');
        $event->deskripsi = $request->input('deskripsi');
        $event->diterima = 0;
        if ($event->save()) {
            return $this->jsonResponseV2(true, null);
        }
        return $this->jsonResponseV2(false, null, ["event" => "gagal ditambahkan ke database"], 500);
    }
}