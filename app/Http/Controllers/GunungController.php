<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 7:04 AM
 */

namespace App\Http\Controllers;


use App\Models\File;
use App\Models\Gunung;
use App\Models\GunungPopular;
use App\Models\GunungReview;
use App\Models\RekomendasiAlat;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GunungController extends Controller
{
    public function reviewGunung(Request $request)
    {
        $this->validate($request,
            [
                'gunung_id' => 'required',
                'bintang' => 'required|max:5',
                'comment' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'max' => ':attribute hanya boleh sampai :max'
            ]);

        $token = $this->parseRequestHeaderStringTokenToToken($request);

        $review = new Review();
        $review->user_id = $token->getClaim('id');
        $review->bintang = $request->json("bintang");
        $review->comment = $request->json("comment");
        $saveReview = $review->save();

        $gunungReview = new GunungReview();
        $gunungReview->gunung_id = $request->json("gunung_id");
        $gunungReview->review()->associate($review);
        $saveGunungReview = $gunungReview->save();

        if ($saveReview && $saveGunungReview) {
            return $this->jsonResponse(null, false, "berhasil menyimpan review gunung");
        }

        return $this->jsonResponse(null, true, "gagal menyimpan review gunung", 500);
    }

    public function cariGunung(Request $request)
    {
        $this->validate(
            $request,
            [
                'nama' => 'required|max:255'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'max' => ':attribute hanya boleh sampai :max'
            ]
        );

        $gunung = Gunung::where('nama', 'like', '%' . $request->json("nama") . '%')->with('file')->get();

        if (count($gunung) == 0) {
            return $this->jsonResponse(null, true, "gunung dengan nama tersebut tidak dapat ditemukan", 500);
        }
        return $this->jsonResponse([
            "gunung" => $gunung
        ], false, "berhail mengambil gunung berdasarkan pencarian nama");
    }

    public function getPopuler()
    {
        $gunung = Gunung::orderByDesc('popular_count')->with('file')->get();

        if ($gunung) {
            return $this->jsonResponse([
                'gunung' => $gunung
            ], false, "berhasil mengambil gunung popular");
        }

        return $this->jsonResponse(null, true, "tidak ada gunung popular", 500);
    }

    public function getGunung(Request $request)
    {
        $this->validate($request, [
            'gunung_id' => 'required'
        ]);

        $gunung = Gunung::with(['file', 'gunungReview.review'])->find($request->json("gunung_id"));

        if ($gunung) {
            return $this->jsonResponse([
                'gunung' => $gunung
            ], false, "berhasil mendapatkan gunung berdasarkan id");
        }

        return $this->jsonResponse(null, true, "gunung dengan id tersebut tidak dapat ditemukan", 422);
    }

    public function semuaGunung()
    {
        $gunung = Gunung::with(['file', 'gunungReview.review'])->get();

        if ($gunung) {
            return $this->jsonResponse([
                'gunung' => $gunung
            ], false, "berhasil mendapatkan semua gunung");
        }

        return $this->jsonResponse(null, true, "gagal mendapatkan semua gunung", 500);
    }

    public function preparation(Request $request)
    {
        $this->validate($request, [
            'gunung_id' => 'required',
            'jumlah_pendaki' => 'required'
        ]);

        $rekomendasiAlat = RekomendasiAlat::with('file')->where('gunung_id', $request->json("gunung_id"))->get();

        for ($i = 0; $i < count($rekomendasiAlat); $i++) {
            $rekomendasiAlat[$i]->jumlah = $this->countItemsNeeded($rekomendasiAlat[$i]->jumlah, $rekomendasiAlat[$i]->orang, $request->json("jumlah_pendaki"));
        }

        if ($rekomendasiAlat) {
            return $this->jsonResponse([
                "preparation" => $rekomendasiAlat
            ], false, "berhasil mendapatkan preparation berdasarkan gunung_id dan jumlah pendaki");
        }

        return $this->jsonResponse(null, true, "tidak ada preparation untuk gunung dan jumlah orang tersebut", 422);
    }

    private function countItemsNeeded($jumlahAlat, $alatUntukOrang, $jumlahOrang)
    {
        while (true) {
            if ($alatUntukOrang >= $jumlahOrang) {
                break;
            }
            ++$jumlahAlat;
            $jumlahOrang -= $alatUntukOrang;
        }

        return $jumlahAlat;
    }
}