<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/23/2018
 * Time: 11:08 PM
 */

namespace App\Http\Controllers;

use App\Models\ProfilePorter;
use Illuminate\Http\Request;

class PorterController extends Controller
{
    public function index()
    {
        $allPorter = ProfilePorter::all();

        if ($allPorter) {
            return $this->jsonResponse([
                'porter' => $allPorter
            ], false, 'berhasil mendapatkan semua porter');
        }

        return $this->jsonResponse(null, true, 'tidak ada porter', 404);
    }

    public function cariPorter(Request $request)
    {
        $this->validate(
            $request,
            [
                'nama' => 'required|string'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'string' => ':attribute harus berupa string'
            ]
        );

        $porter = ProfilePorter::where('nama', 'like', '%' . $request->json('nama') . '%')->get();

        if ($porter) {
            return $this->jsonResponse([
                'porter' => $porter
            ], false, 'berhasil mendapatkan porter sesuai nama');
        }

        return $this->jsonResponse(null, true, 'porter dengan nama tersebut tidak ditemukan', 404);
    }
}