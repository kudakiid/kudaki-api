<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function loginPendaki(Request $request)
    {
        $this->validate(
            $request,
            [
                'username' => 'max:255|exists:users,username',
                'email' => 'email|max:255|exists:users,email',
                'password' => 'required'
            ],
            [
                'exists' => 'login pendaki gagal, :attribute tidak terdaftar',
                'required' => ':attribute tidak boleh kosong',
                'email' => ':attribute harus diisi dengan email yang valid'
            ]);

        if ($request->has("username")) {
            $user = User::where('username', $request->json("username"))->first();
        } else if ($request->has("email")) {
            $user = User::where('email', $request->json("email"))->first();
        }

        if (Hash::check($request->json("password"), $user->password)) {
            $user->token = (string)$this->generateUserToken($user);
            $user->save();
            return $this->jsonResponse([
                "token" => (string)$user->token
            ], false, "login pendaki berhasil, dan token berhasil di refresh");
        }

        return $this->jsonResponse(null, true, "login pendaki salah, password salah", 500);
    }

    public function logoutPendaki(Request $request)
    {
        $token = $this->parseStringToToken($request->header("X-Ekspedia-Token"));
        $user = User::find($token->getClaim('id'));
        $user->token = "";
        $saveUser = $user->save();

        if ($saveUser) {
            return $this->jsonResponse(null, false, "berhasil logout pendaki");
        }

        return $this->jsonResponse(null, true, "gagal logout pendaki", 500);
    }


}
