<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;
use Laravel\Lumen\Routing\Controller as BaseController;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

class Controller extends BaseController
{
    protected $signer;
    protected $validationData;
    protected $publicStoragePath;

    function __construct()
    {
        $this->signer = new Sha256();
        $this->validationData = new ValidationData();
        $this->publicStoragePath = str_replace("\\", "/", storage_path()) . '/app/public/';
    }

    function jsonResponse($body, $error, $message, $responseCode = 200, $headers = [])
    {
        return response()->json([
            "data" => $body,
            "error" => $error,
            "message" => $message
        ], $responseCode, $headers);
    }

    function jsonResponseV2($success, $body, $errors = [], $responseCode = 200, $headers = [])
    {
        return response()->json([
            "success" => $success,
            "data" => $body,
            "errors" => $errors
        ], $responseCode, $headers);
    }

    protected function formatValidationErrors(Validator $validator)
    {
        return [
            "data" => null,
            "error" => true,
            "message" => $validator->errors()->all()
        ];
    }

    public function generateUserToken(User $user)
    {
        $token = (new Builder())
            ->setIssuedAt(time())
            ->setExpiration(time() + 172800)
            ->set('id', $user->id)
            ->set('username', $user->username)
            ->set('email', $user->email)
            ->set('role_id', $user->role()->first()->id)
            ->sign($this->signer, 'alrhg340t2u305uwiuegf23bwefih284')
            ->getToken();

        return $token;
    }

    protected function parseStringToToken($tokenString)
    {
        return (new Parser())->parse((string)$tokenString);
    }

    protected function parseRequestHeaderStringTokenToToken(Request $request)
    {
        return $this->parseStringToToken((string)$request->header('X-Ekspedia-Token'));
    }

    protected function generateRandomNumber()
    {
        $keySpace = '0123456789';

        $str = '';
        $max = mb_strlen($keySpace, '8bit') - 1;
        for ($i = 0; $i < 6; ++$i) {
            $str .= $keySpace[random_int(0, $max)];
        }

        return $str;
    }
}
