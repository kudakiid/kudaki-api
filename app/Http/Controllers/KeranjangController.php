<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 10:41 AM
 */

namespace App\Http\Controllers;


use App\Events\KeranjangBerubah;
use App\Models\Barang;
use App\Models\BarangKeranjang;
use App\Models\Checkout;
use App\Models\Keranjang;
use Illuminate\Http\Request;

class KeranjangController extends Controller
{
    public function index(Request $request)
    {
        $keranjang = Keranjang::with(['barangKeranjang', 'barangKeranjang.barang', 'barangKeranjang.barang.file'])
            ->where([
                ['id', $request->attributes->get('keranjang')->id],
                ['user_id', $request->user()->id]
            ])->get();

        if ($keranjang) {
            return $this->jsonResponse([
                'barang_keranjang' => $keranjang
            ], false, "berhasil mengambil keranjang beserta barang");
        }

        return $this->jsonResponse(null, true, "tidak ada keranjang terbuka dengan user id tersebut", 422);
    }

    public function tambahBarang(Request $request)
    {
        $this->validate(
            $request,
            [
                'barang_id' => 'required|exists:barangs,id',
                'total_barang' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'exists' => 'barang dengan id tersebut tidak ada'
            ]
        );

        $barang = Barang::find($request->json('barang_id'));

        $barangKeranjang = BarangKeranjang::where([
            ['keranjang_id', $request->get('keranjang')->id],
            ['barang_id', $barang->id]
        ])->first();

        if ($barangKeranjang) {
            $barangKeranjang->total_barang = $barangKeranjang->total_barang + $request->json("total_barang");
            $barangKeranjang->total_harga = $barangKeranjang->total_barang * $barang->harga;
            $saveBarangKeranjang = $barangKeranjang->save();

            if ($saveBarangKeranjang) {
                event(new KeranjangBerubah(
                    $request->get('keranjang')->id,
                    $request->json('total_barang'),
                    $request->json('total_barang') * $barang->harga,
                    '+'
                ));
                return $this->jsonResponse(null, false, "barang sudah ada, berhasil menambahkan harga");
            }

            return $this->jsonResponse(null, true, "gagal menambahkan barang ke keranjang", 500);
        }

        $saveNewBarangKeranjang = BarangKeranjang::saveNewBarangKeranjang($request, $barang);

        if ($saveNewBarangKeranjang) {
            event(new KeranjangBerubah(
                $request->get('keranjang')->id,
                $request->json('total_barang'),
                $request->json('total_barang') * $barang->harga,
                '+'
            ));
            return $this->jsonResponse(null, false, "berhasil menambahkan barang ke keranjang");
        }

        return $this->jsonResponse(null, true, "gagal menambahkan barang ke keranjang", 500);
    }

    public function editBarang(Request $request)
    {
        $this->validate(
            $request,
            [
                'barang_id' => 'required|exists:barangs,id',
                'total_barang' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'exists' => ':table dengan :column tersesbut tidak ada'
            ]
        );


    }

    public function deleteBarang(Request $request)
    {
        $this->validate(
            $request,
            [
                'barang_keranjang_id' => 'required|exists:barang_keranjangs,id',
                'total_barang' => 'required'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'exists' => 'barang tersebut tidak ada didalam keranjang'
            ]
        );

        $barangKeranjang = BarangKeranjang::find($request->json("barang_keranjang_id"));

        if ($barangKeranjang) {
            $barangKeranjang->delete();
            event(new KeranjangBerubah(
                $request->get('keranjang')->id,
                $barangKeranjang->total_barang,
                $barangKeranjang->total_harga,
                '-'
            ));
            return $this->jsonResponse(null, false, "berhasil hapus barang dari keranjang");
        }

        return $this->jsonResponse(null, true, "barang tidak ada didalam keranjang", 500);
    }

    public function checkout(Request $request)
    {
        $this->validate(
            $request,
            [
                'payment_method_id' => 'required|exists:payment_methods,id'
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'payment_method_id.exists' => 'payment method dengan id tersebut tidak ada'
            ]
        );

        $checkout = new Checkout();
        $checkout->keranjang_id = $request->get('keranjang')->id;
        $checkout->payment_method_id = $request->json('payment_method_id');
        $checkout->id_transaksi = $this->generateRandomNumber();
        $checkout->issued_pada = date('Y-m-d H:i:s', time());
        $saveCheckout = $checkout->save();

        if ($saveCheckout) {
            $keranjang = Keranjang::find($request->get('keranjang')->id);
            $keranjang->open = false;
            $keranjang->save();
            return $this->jsonResponse(null, false, "berhasil melakukan checkout keranjang");
        }

        return $this->jsonResponse(null, true, "gagal melakukan checkout keranjang", 500);
    }
}