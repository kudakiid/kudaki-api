<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 12:13 PM
 */

namespace App\Http\Controllers;


use App\Models\PaymentMethod;

class PaymentMethodController extends Controller
{
    public function index()
    {
        $paymentMethod = PaymentMethod::all();

        if ($paymentMethod) {
            return $this->jsonResponse([
                "payment_method" => $paymentMethod
            ], false, "berhasil mengambil semua payment method");
        }

        return $this->jsonResponse(null, true, "tidak ada payment method", 500);
    }
}