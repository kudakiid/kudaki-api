<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 10:19 PM
 */

namespace App\Http\Controllers;


use App\Models\Lokasi;
use Illuminate\Http\Request;

class LokasiController extends Controller
{
    public function semuaLokasi()
    {
        $lokasi = Lokasi::all();

        if ($lokasi) {
            return $this->jsonResponse([
                "lokasi" => $lokasi
            ], false, "berhasil mengambil semua lokasi");
        }

        return $this->jsonResponse(null, true, "tidak ada lokasi", 500);
    }

    public function cariLokasi(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required'
        ]);

        $lokasi = Lokasi::where('nama', 'like', '%' . $request->json("nama") . '%')->get();
        if ($lokasi) {
            return $this->jsonResponse([
                'lokasi' => $lokasi
            ], false, "berhasil mendapatkan lokasi dengan nama tersebut");
        }

        return $this->jsonResponse(null, true, "lokasi dengan nama tersebut tidak ditemukan", 422);
    }
}