<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/14/2018
 * Time: 12:27 PM
 */

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Lcobucci\JWT\Parser;

class BaseMiddleware
{
    protected $token;

    protected function parseStringToToken($tokenString)
    {
        return (new Parser())->parse((string)$tokenString);
    }

    protected function parseRequestHeaderStringTokenToToken(Request $request)
    {
        return $this->parseStringToToken((string)$request->header('X-Ekspedia-Token'));
    }
}