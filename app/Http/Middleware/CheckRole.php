<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 3/2/2018
 * Time: 11:20 AM
 */

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    public function handle($request, Closure $next, $role)
    {
        if ($request->user()->role()->first()->nama == $role) {
            return $next($request);
        }

        return response()->json([
            'message' => 'Your role isn\'t authorized to do this action',
            'your_role' => $request->user()->role->name
        ]);
    }
}