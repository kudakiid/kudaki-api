<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:59 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    protected $table = 'keranjangs';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'id', 'user_id', 'total_barang', 'total_harga', 'open'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function barangKeranjang()
    {
        return $this->hasMany('App\Models\BarangKeranjang', 'keranjang_id', 'id');
    }

    public function checkout()
    {
        return $this->hasOne('App\Models\Checkout', 'keranjang_id', 'id');
    }
}
