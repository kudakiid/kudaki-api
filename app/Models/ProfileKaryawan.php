<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:02 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfileKaryawan extends Model
{
    protected $table = 'profilfe_karyawans';

    protected $fillable = [
        'id', 'user_id', 'toko_id', 'file_id', 'jabatan', 'nama', 'telepon', 'tempat_lahir' . 'tanggal_lahir'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function toko()
    {
        return $this->belongsTo('App\Models\Toko', 'toko_id', 'id');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}