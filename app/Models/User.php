<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'token', 'activated'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

//    public function roleUser()
//    {
//        return $this->hasMany('App\Models\RoleUser', 'user_id', 'id');
//    }

    public function role()
    {
        return $this->belongsToMany('App\Models\Role', 'role_users', 'user_id', 'role_id');
    }

    public function profilePendaki()
    {
        return $this->hasOne('App\Models\ProfilePendaki', 'user_id', 'id');
    }

    public function profileKaryawan()
    {
        return $this->hasOne('App\Models\ProfileKaryawan', 'user_id', 'id');
    }

    public function keranjang()
    {
        return $this->hasOne('App\Models\Keranjang', 'user_id', 'id');
    }

    public function profilePorter()
    {
        return $this->hasOne('App\Models\ProfilePorter', 'user_id', 'id');
    }

    public function review()
    {
        return $this->hasMany('App\Models\Review', 'user_id', 'id');
    }
}
