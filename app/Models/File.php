<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:43 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';

    protected $fillable = [
        'id', 'nama', 'path', 'type', 'size'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'pivot'
    ];

    public function menu()
    {
        return $this->hasOne('App\Models\Menu', 'file_id', 'id');
    }

    public function profilePendaki()
    {
        return $this->hasOne('App\Models\ProfilePendaki', 'file_id', 'id');
    }

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'file_id', 'id');
    }

    public function profilePorter()
    {
        return $this->hasOne('App\Models\ProfilePorter', 'file_id', 'id');
    }

    public function gunung()
    {
        return $this->belongsToMany('App\Models\Gunung', 'gunung_files', 'file_id', 'gunung_id');
    }

    public function jalur()
    {
        return $this->hasOne('App\Models\Jalur', 'file_id', 'id');
    }

    public function track()
    {
        return $this->hasOne('App\Models\Track', 'file_id', 'id');
    }

    public function rekomendasiAlat()
    {
        return $this->hasOne('App\Models\RekomendasiAlat', 'file_id', 'id');
    }

    public function profileKaryawan()
    {
        return $this->hasOne('App\Models\ProfileKaryawan', 'file_id', 'id');
    }

    public function fileToko()
    {
        return $this->hasOne('App\Models\FileToko', 'file_id', 'id');
    }

    public function kategori()
    {
        return $this->hasOne('App\Models\Kategori', 'file_id', 'id');
    }

    public function barang()
    {
        return $this->hasOne('App\Models\Barang', 'file_id', 'id');
    }

    public function bank()
    {
        return $this->hasOne('App\Models\Bank', 'file', 'id');
    }
}