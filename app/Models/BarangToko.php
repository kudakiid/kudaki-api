<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:10 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BarangToko extends Model
{
    protected $table = 'barang_tokos';

    protected $fillable = [
        'id', 'toko_id', 'barang_id', 'jumlah', 'jumlah_unit'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function barang()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id', 'id');
    }

    public function toko()
    {
        return $this->belongsTo('App\Models\Toko', 'toko_id', 'id');
    }
}