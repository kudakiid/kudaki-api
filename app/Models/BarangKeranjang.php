<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:14 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BarangKeranjang extends Model
{
    protected $table = 'barang_keranjangs';

    protected $fillable = [
        'id', 'keranjang_id', 'barang_id', 'total_barang', 'total_harga'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function keranjang()
    {
        return $this->belongsTo('App\Models\Keranjang', 'keranjang_id', 'id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Models\Barang', 'barang_id', 'id');
    }

    public static function saveNewBarangKeranjang(Request $request, Barang $barang)
    {
        $newBarangKeranjang = new BarangKeranjang();
        $newBarangKeranjang->keranjang_id = $request->get('keranjang')->id;
        $newBarangKeranjang->barang_id = $request->json('barang_id');
        $newBarangKeranjang->total_barang = $request->json('total_barang');
        $newBarangKeranjang->total_harga = $request->json('total_barang') * $barang->harga;
        return $newBarangKeranjang->save();
    }
}