<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 2:18 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = 'checkouts';

    public $timestamps = false;

    protected $fillable = [
        'id', 'keranjang_id', 'payment_method_id', 'id_transaksi', 'issued_at'
    ];

    public function keranjang()
    {
        return $this->belongsTo('App\Models\Keranjang', 'keranjang_id', 'id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id', 'id');
    }
}