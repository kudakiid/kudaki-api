<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:55 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class FileToko extends Model
{
    protected $table = 'file_tokos';

    protected $fillable = [
        'id', 'file_id', 'toko_id', 'nama'
    ];

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }

    public function toko()
    {
        return $this->belongsTo('App\Models\Toko', 'toko_id', 'id');
    }
}