<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:40 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'lokasis';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'id', 'nama'
    ];

    public function toko()
    {
        return $this->hasMany('App\Models\Toko', 'lokasi_id', 'id');
    }
}