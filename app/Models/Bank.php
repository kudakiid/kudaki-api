<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 11:57 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'banks';

    protected $fillable = [
        'id', 'file_id', 'nama', 'rekening'
    ];

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }
}