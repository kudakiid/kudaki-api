<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/23/2018
 * Time: 11:13 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfilePorter extends Model
{
    protected $fillable = [
        'id', 'file_id', 'user_id', 'nama', 'alamat', 'telepon', 'usia', 'gender'
    ];

    protected $hidden = ['created_at', 'updated_at'];
}