<?php
/**
 * Created by PhpStorm.
 * User=> milha
 * Date=> 11/30/2018
 * Time=> 10=>02 PM
 */

namespace App\Models\Line;


class Event
{
    public function format($events)
    {
        $theEvents = array();
        foreach ($events as $event) {
            array_push($theEvents, $this->event($event));
        }

        $formatted = array(
            "type" => "carousel",
            "contents" => $theEvents
        );

        return $formatted;
//        $encoded = json_encode($formatted);
//
//        echo "THE ENCODED EVENTS : " . $encoded;
//        return $encoded;
    }

    public function event(\App\Models\Event $event)
    {
        $event = array(
            "type" => "bubble",
            "hero" => array(
                "type" => "image",
                "url" => "https://images.unsplash.com/photo-1485967249725-2d0b975fa8a2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80",
                "size" => "full",
                "aspectRatio" => "20:13",
                "aspectMode" => "cover"
            ),
            "body" => array(
                "type" => "box",
                "layout" => "vertical",
                "contents" => array(
                    array(
                        "type" => "text",
                        "text" => $event->nama,
                        "weight" => "bold",
                        "size" => "xl"
                    ),
                    array(
                        "type" => "box",
                        "layout" => "vertical",
                        "margin" => "lg",
                        "spacing" => "sm",
                        "contents" => array(
                            array(
                                "type" => "box",
                                "layout" => "baseline",
                                "spacing" => "sm",
                                "contents" => array(
                                    array(
                                        "type" => "text",
                                        "text" => "By",
                                        "color" => "#aaaaaa",
                                        "size" => "sm",
                                        "flex" => 1
                                    ),
                                    array(
                                        "type" => "text",
                                        "text" => $event->penyelenggara,
                                        "wrap" => true,
                                        "color" => "#666666",
                                        "size" => "sm",
                                        "flex" => 5
                                    ),
                                ),
                            ),
                            array(
                                "type" => "box",
                                "layout" => "baseline",
                                "spacing" => "sm",
                                "contents" => array(
                                    array(
                                        "type" => "text",
                                        "text" => "Place",
                                        "color" => "#aaaaaa",
                                        "size" => "sm",
                                        "flex" => 1
                                    ),
                                    array(
                                        "type" => "text",
                                        "text" => $event->tempat,
                                        "wrap" => true,
                                        "color" => "#666666",
                                        "size" => "sm",
                                        "flex" => 5
                                    ),
                                ),
                            ),
                            array(
                                "type" => "box",
                                "layout" => "baseline",
                                "spacing" => "sm",
                                "contents" => array(
                                    array(
                                        "type" => "text",
                                        "text" => "Start",
                                        "color" => "#aaaaaa",
                                        "size" => "sm",
                                        "flex" => 1
                                    ),
                                    array(
                                        "type" => "text",
                                        "text" => $event->tanggal_mulai,
                                        "wrap" => true,
                                        "color" => "#666666",
                                        "size" => "sm",
                                        "flex" => 5
                                    ),
                                ),
                            ),
                            array(
                                "type" => "box",
                                "layout" => "baseline",
                                "spacing" => "sm",
                                "contents" => array(
                                    array(
                                        "type" => "text",
                                        "text" => "End",
                                        "color" => "#aaaaaa",
                                        "size" => "sm",
                                        "flex" => 1
                                    ),
                                    array(
                                        "type" => "text",
                                        "text" => $event->tanggal_selesai,
                                        "wrap" => true,
                                        "color" => "#666666",
                                        "size" => "sm",
                                        "flex" => 5
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            "footer" => array(
                "type" => "box",
                "layout" => "vertical",
                "spacing" => "sm",
                "contents" => array(
                    array(
                        "type" => "button",
                        "style" => "link",
                        "height" => "sm",
                        "action" => array(
                            "type" => "uri",
                            "label" => "DETAIL",
                            "uri" => "http://kudaki.id"
                        )
                    ),
                    array(
                        "type" => "spacer",
                        "size" => "sm"
                    ),
                ),
                "flex" => 0
            )
        );

        return $event;
    }
}