<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:42 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Jalur extends Model
{
    protected $table = 'jalurs';

    protected $fillable = [
        'id', 'gunung_id', 'file_id', 'nama'
    ];

    public function file()
    {
        return $this->belongsTo('App\Models\File', 'file_id', 'id');
    }

    public function gunung_id()
    {
        return $this->belongsTo('App\Models\Gunung', 'gunung_id', 'id');
    }
}