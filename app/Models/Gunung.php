<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/9/2018
 * Time: 10:50 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Gunung extends Model
{
    protected $table = 'gunungs';

    public $timestamps = false;

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $fillable = [
        'id', 'nama', 'ketinggian', 'latitude', 'longitude', 'deskripsi', 'bintang_review', 'kesulitan', 'popular_count'
    ];

    public function jalur()
    {
        return $this->hasMany('App\Models\Jalur', 'gunung_id', 'id');
    }

    public function file()
    {
        return $this->belongsToMany('App\Models\File', 'gunung_files', 'gunung_id', 'file_id');
    }

    public function gunungPorter()
    {
        return $this->hasMany('App\Models\GunungPorter', 'gunung_id', 'id');
    }

    public function gunungReview()
    {
        return $this->hasMany('App\Models\GunungReview', 'gunung_id', 'id');
    }

    public function rekomendasiAlat()
    {
        return $this->hasMany('App\Models\RekomendasiAlat', 'gunung_id', 'id');
    }

    public function track()
    {
        return $this->belongsToMany('App\Models\Track', 'gunung_tracks', 'gunung_id', 'track_id');
    }
}