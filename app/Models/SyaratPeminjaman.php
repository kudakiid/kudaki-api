<?php
/**
 * Created by PhpStorm.
 * User: milha
 * Date: 4/10/2018
 * Time: 1:58 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class SyaratPeminjaman extends Model
{
    protected $table = 'syarat_peminjamans';

    protected $fillable = [
        'id', 'toko_id', 'persyaratan', 'jumlah', 'deskripsi'
    ];

    public function toko()
    {
        return $this->belongsTo('App\Models\Toko', 'toko_id', 'id');
    }
}