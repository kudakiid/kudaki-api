<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {

            if ($request->headers->get('X-Ekspedia-Token')) {
                if (!(new Parser())->parse((string)$request->headers->get('X-Ekspedia-Token'))->validate(new ValidationData())) {
                    return null;
                }
                $token = (new Parser())->parse((string)$request->headers->get('X-Ekspedia-Token'));
                $user = User::find($token->getClaim('id'));
                if ($user) {
                    if ($user->token == $request->headers->get('X-Ekspedia-Token')) {
                        if ($token->verify(new Sha256(), 'alrhg340t2u305uwiuegf23bwefih284')) {
                            return $user;
                        }
                    }
                }
            }
            return null;
        });
    }
}
